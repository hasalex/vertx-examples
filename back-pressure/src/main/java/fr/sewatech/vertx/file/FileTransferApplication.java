package fr.sewatech.vertx.file;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.FileSystem;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpServer;

import java.lang.management.BufferPoolMXBean;
import java.lang.management.ManagementFactory;
import java.util.List;

public class FileTransferApplication {

    private static final String BIG_FILE_PATH = System.getProperty("java.io.tmpdir") + "/big-file.bin";
    private static Vertx vertx;

    public static void main(String[] args) {
        vertx = Vertx.vertx();
//        downloadBigFile(nothing -> startHttpServer());
        mainBis();
    }

    private static void mainBis() {
        System.gc();
        readBigFile(buffer -> {
            printMemory(true);
            mainBis();
        });
    }

    private static void startHttpServer() {
        HttpServer httpServer = vertx.createHttpServer();
        httpServer
                .requestHandler(request -> {
                    System.gc();
                    printMemory(false);
                    request.response().setStatusCode(200);
                    readBigFile(buffer -> {
                        printMemory(true);
                        if (buffer == null) {
                            request.response().end();
                        } else {
                            request.response().end();
                        }
                    });
                })
                .listen(8888, event -> startHttpClients());
//        vertx.close();
    }

    private static void printMemory(boolean end) {
//        long usedMemory = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed() / 1024 / 1024;
//        System.out.println((end ? "-> " : "") + usedMemory);
        List<BufferPoolMXBean> pools = ManagementFactory.getPlatformMXBeans(BufferPoolMXBean.class);
        pools.stream()
                .filter(pool -> "direct".equals(pool.getName()))
                .forEach(pool -> System.out.printf("%s %s %s \n", end ? "-> " : "", pool.getCount(), pool.getMemoryUsed() / 1024 / 1024));
    }

    private static void startHttpClients() {
        startHttpClient();
//        startHttpClient();
//        startHttpClient();
//        startHttpClient();
//        startHttpClient();
    }

    private static void startHttpClient() {
        HttpClient httpClient = vertx.createHttpClient();
        httpClient
                .get(8888, "localhost", "/")
                .handler(response -> {
//                    response.pause();
                })
                .end();
    }

    private static void readBigFile(Handler<Buffer> handler) {
        FileSystem fs = vertx.fileSystem();
        fs.readFile(BIG_FILE_PATH, event -> {
            if (event.succeeded()) {
                handler.handle(event.result());
            } else {
                event.cause().printStackTrace();
                vertx.close();
            }
        });
    }

    private static void downloadBigFile(Handler<Void> handler) {
        FileSystem fs = vertx.fileSystem();
        fs.exists(BIG_FILE_PATH, event -> {
            if (event.failed() || !event.result()) {
                System.out.println("No big file found at " + BIG_FILE_PATH);
                tryDownload(handler);
            } else {
                System.out.println("Big file found at " + BIG_FILE_PATH);
                handler.handle(null);
            }
        });
    }

    private static void tryDownload(Handler<Void> endHandler) {
        FileSystem fs = vertx.fileSystem();
        fs.createFileBlocking(BIG_FILE_PATH);
        AsyncFile file = fs.openBlocking(BIG_FILE_PATH, new OpenOptions().setAppend(true));
        HttpClientOptions options = new HttpClientOptions().setLogActivity(true);
        vertx.createHttpClient(options).getNow(
                80,
                "linuxsoft.cern.ch",
                "/centos/8/isos/x86_64/CentOS-8.1.1911-x86_64-boot.iso",
                response -> response
                        .handler(file::write)
                        .endHandler(event -> {
                            file.close();
                            endHandler.handle(null);
                        }));
    }
}
