package fr.sewatech.vertx.file;

import fr.sewatech.vertx.file.config.Configuration;
import fr.sewatech.vertx.file.files.IOFunction;
import fr.sewatech.vertx.file.reporting.Report;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;

import java.nio.file.Path;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import static fr.sewatech.vertx.file.files.BigFiles.*;
import static fr.sewatech.vertx.file.reporting.MemoryReport.reportMemoryConfiguration;
import static fr.sewatech.vertx.file.reporting.MemoryReport.reportMemoryUsage;

public class ReadFileApplication extends AbstractVerticle {
    static final Vertx vertx = Vertx.vertx();

    private final Configuration configuration;
    private final Report report = new Report();
    private final RunMode runMode;
    private long startTime;

    public ReadFileApplication(RunMode runMode) {
        this.runMode = runMode;

        this.configuration = Configuration.build();

        this.report.putMemoryConfiguration(reportMemoryConfiguration())
                .putLoops(configuration.loops())
                .putRunMode(runMode.name());
    }

    public static void main(String[] args) {
        vertx.deployVerticle(new ReadFileApplication(RunMode.init(args)));
    }

    public void start() {
        // Attention: blocking
        initBigFiles();

        startTime = System.currentTimeMillis();
        this.readBigFiles(0);
    }

    private void readBigFiles(int count) {
        if (count < MAX_FILES * configuration.loops()) {
            readBigFile(count, () -> readBigFiles(count + 1));
        } else {
            shutdown();
        }
    }

    private void readBigFile(int count, Runnable callback) {
        runMode.function.accept(getPath("-" + count % MAX_FILES), ar -> {
            if (ar.succeeded()) {
                try {
                    report.putSomeBytes(extractSomeBytes(ar.result().getBytes()));
                    callback.run();
                } catch (Throwable e) {
                    e.printStackTrace();
                    shutdown();
                }
            } else {
                ar.cause().printStackTrace();
                shutdown();
            }
        });
    }

    private void shutdown() {
        report.putMemoryUsage(reportMemoryUsage());
        report.putDuration(System.currentTimeMillis() - startTime);

        vertx.close();
        report.print(configuration.reportConfiguration());
    }


    private enum RunMode {
        READ_FILE(ReadFileActions::readFile),
        READ_FILE_BLOCKING(ReadFileBlockingActions::readFileBlocking),
        OPEN_FILE(ReadFileActions::openFile),
        FILE_CHANNEL(ReadFileBlockingActions::fileChannel);

        private final BiConsumer<Path, Handler<AsyncResult<Buffer>>> function;

        RunMode(BiConsumer<Path, Handler<AsyncResult<Buffer>>> function) {
            this.function = function;
        }
        RunMode(IOFunction<Path, Buffer> function) {
            this.function = (path, handler) -> vertx.executeBlocking(promise -> function.applyWithPromise(path, promise), handler);
        }

        static RunMode init(String[] args) {
            return Stream.of(args)
                    .findFirst()
                    .map(text -> text.replace('-', '_'))
                    .map(String::toUpperCase)
                    .map(RunMode::valueOf)
                    .orElse(READ_FILE);
        }
    }
}
