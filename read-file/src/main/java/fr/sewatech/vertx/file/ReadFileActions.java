package fr.sewatech.vertx.file;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.OpenOptions;

import java.nio.file.Path;

import static fr.sewatech.vertx.file.ReadFileApplication.vertx;

public interface ReadFileActions {

    static void readFile(Path path, Handler<AsyncResult<Buffer>> handler) {
        vertx.fileSystem()
                .readFile(path.toString(), handler);
    }

    static void openFile(Path path, Handler<AsyncResult<Buffer>> handler) {
        int size = (int) path.toFile().length();
        int bufferSize = 64 * 1024;

        long startTime = System.currentTimeMillis();
        Buffer result = Buffer.buffer(size); // important, sinon perf de merde
        vertx.fileSystem()
                .open(
                        path.toString(),
                        new OpenOptions().setRead(true),
                        ar -> {
                            try {
                                if (ar.succeeded()) {
                                    AsyncFile file = ar.result().setReadBufferSize(bufferSize);
                                    file
                                            .handler(result::appendBuffer)
                                            .endHandler(nothing -> handler.handle(Future.succeededFuture(result)))
                                            .exceptionHandler(throwable -> handler.handle(Future.failedFuture(throwable)));
                                } else {
                                    handler.handle(Future.failedFuture(ar.cause()));
                                }
                            } catch (Throwable throwable) {
                                handler.handle(Future.failedFuture(throwable));
                            }
                        });
    }
}